﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;

namespace WpfApplication3 {
  /// <summary>
  /// MainWindow.xaml の相互作用ロジック
  /// </summary>
  public partial class MainWindow : Window {
    class PointContiner {
      public enum PointTypes {
        Translation,
        RotationRoll,
      }
      public PointTypes PointType { get; set; }
      public Point Value { get; set; }
    }

    PointContiner mousePositionBuf = null;

    public MainWindow() {
      InitializeComponent(); 
      {
        string s = this.Title;
        int n = 0;
        DispatcherTimer t = new DispatcherTimer();
        t.Interval = TimeSpan.FromSeconds(1);
        t.Tick += delegate (object sender, EventArgs e) {
          this.Title = string.Format("{0}{1}", s, n);
          n++;
          n %= 10;
        };
        t.Start();
      }
      {
        ImageSource icon;
        if(this.Icon == null) {
          using(System.IO.MemoryStream s = new System.IO.MemoryStream()) {
            System.Drawing.SystemIcons.Application.Save(s);
            icon = BitmapFrame.Create(s);
          }
        } else {
          icon = this.Icon;
        }
        imageAppIcon.Source = icon;
      }
    }

    #region[methods]
    private void fixStateStyle() {
      if(this.WindowState == System.Windows.WindowState.Maximized) {
        BorderThickness = new Thickness(6);
        this.buttonNormalize.Visibility= System.Windows.Visibility.Visible;
        this.buttonMaximize.Visibility = System.Windows.Visibility.Collapsed;
      } else {
        BorderThickness = new Thickness(0);
        this.buttonNormalize.Visibility= System.Windows.Visibility.Collapsed;
        this.buttonMaximize.Visibility = System.Windows.Visibility.Visible;
      }
    }

    private void OpenImageFile(string path) {
      BitmapImage img = new BitmapImage();
      using(FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
        img.BeginInit();
        img.CacheOption = BitmapCacheOption.OnLoad;
        img.StreamSource = stream;
        img.EndInit();
        image.Source = img;
      }
    }

    private void Message(string s) {
      const int MAX_LINE = 100;
      StringBuilder sb = new StringBuilder();
      sb.AppendLine(s);
      using(StringReader r = new StringReader(textBox.Text)) {
        for(int i = 0; i < MAX_LINE - 1; i++) {
          sb.AppendLine(r.ReadLine());
        }
      }
      textBox.Text = sb.ToString();
    }

    #endregion

    #region [event]
    private void window_Initialized(object sender, EventArgs e) {
      fixStateStyle();
    }
    private void window_StateChanged(object sender, EventArgs e) {
      fixStateStyle();
    }
    #endregion

    #region [command]
    private void CommandCloseCommand(object sender, RoutedEventArgs e) {
      SystemCommands.CloseWindow(this);
    }
    private void CommandMaximizeWindow(object sender, RoutedEventArgs e) {
      SystemCommands.MaximizeWindow(this);
    }
    private void CommandMinimizeWindow(object sender, RoutedEventArgs e) {
      SystemCommands.MinimizeWindow(this);
    }
    private void CommandRestoreWindow(object sender, RoutedEventArgs e) {
      SystemCommands.RestoreWindow(this);
    }
    #endregion

    #region [menu]
    private void FileMenuOpen(object sender, RoutedEventArgs e) {
      OpenFileDialog dialog = new OpenFileDialog();
      dialog.Filter = "all file|*|all images|*.bmp;*.jpg;*.jpeg;*.png;*.gif;|BMP|*.bmp|JPEG|*.jpg;*.jpeg|PNG|*.png;*.png|GIF|*.gif";
      dialog.FilterIndex = 2;
      if(dialog.ShowDialog() == true) {
        string path = dialog.FileName;
        try {
          OpenImageFile(path);
          Message(string.Format("[MainWindow.FileMenuOpen()] loaded({0})", new object[] { path }));
        }
        catch (Exception ex){
          Message(string.Format("[MainWindow.FileMenuOpen()] {0}", new object[] { ex.ToString() }));
          Message(string.Format("[MainWindow.FileMenuOpen()] faild...({0})", new object[] { path }));
        }
      }
    }
    private void WindowMenuTopMost(object sender, RoutedEventArgs e) {
      this.Topmost = !this.Topmost;
      menuItemTopMost.IsChecked = this.Topmost;
    }
    private void HelpMenuVersion(object sender, RoutedEventArgs e) {
    }
    #endregion

    #region [control]
    private void MouseEventZoomWheel(object sender, MouseWheelEventArgs e) {
      double s = (e.Delta / (120.0 * 3.0));
      double scale;
      //calc scale
      {
        if(s > 0) {
          scale = s;
        } else if(s < 0) {
          scale = 1 / Math.Abs(s);
        } else {
          scale = 1;
        }
      }
      //calc matrix
      {
        Matrix m = matrixTransform.Matrix;
        Matrix m1 = new Matrix();
        m1.Scale(scale, scale);
        m= Matrix.Multiply(m, m1);
        matrixTransform.Matrix = m;
      }
      Message(string.Format("[MainWindow.MouseEventZoomWheel()] delta({0}) s({1}({2}))", new object[] { e.Delta, s, scale }));
    }
      
    private void MouseEventStart(object sender, MouseButtonEventArgs e) {
      if(e.ChangedButton == MouseButton.Left) {
        mousePositionBuf = new PointContiner() { PointType = PointContiner.PointTypes.Translation, Value = e.GetPosition(this) };
        Mouse.OverrideCursor = Cursors.ScrollAll;
        Message(string.Format("[MainWindow.MouseEventStart()] mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
      } else if(e.ChangedButton == MouseButton.Right) {
        mousePositionBuf = new PointContiner() { PointType = PointContiner.PointTypes.RotationRoll, Value = e.GetPosition(this) };
        Mouse.OverrideCursor = Cursors.SizeWE;
        Message(string.Format("[MainWindow.MouseEventStart()] mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
      }
    }

    private void MouseEventMoving(object sender, MouseEventArgs e) {
      if(mousePositionBuf != null) {
        Point pos = e.GetPosition(this);
        switch(mousePositionBuf.PointType) {
          case PointContiner.PointTypes.Translation: {
              Point p = new Point(pos.X - mousePositionBuf.Value.X, pos.Y - mousePositionBuf.Value.Y);
              //calc matrix
              {
                Matrix m = matrixTransform.Matrix;
                TranslateTransform t = new TranslateTransform();
                t.X = p.X;
                t.Y = p.Y;
                m= Matrix.Multiply(m, t.Value);
                matrixTransform.Matrix = m;
              }
              mousePositionBuf.Value = pos;
              Message(string.Format("[MainWindow.MouseEventMoving()] TR mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
              break;
            }
          case PointContiner.PointTypes.RotationRoll: {
              double roll = (pos.X - mousePositionBuf.Value.X);
              //calc matrix
              {
                Matrix m = matrixTransform.Matrix;
                RotateTransform t = new RotateTransform();
                t.Angle = roll;
                m= Matrix.Multiply(m, t.Value);
                matrixTransform.Matrix = m;
              }
              mousePositionBuf.Value = pos;
              Message(string.Format("[MainWindow.MouseEventMoving()] RL mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
              break;
            }
          default: {
              mousePositionBuf = null;
              break;
            }
        }
      }
    }

    private void MouseEventEnd(object sender, MouseButtonEventArgs e) {
      mousePositionBuf = null;
      Mouse.OverrideCursor = null;
      Message(string.Format("[MainWindow.MouseEventEnd()]"));
    }

  }
  #endregion
}
