﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Windows.Forms;
using System.Drawing;

namespace WpfApplication2 {
  /// <summary>
  /// MainWindow.xaml の相互作用ロジック
  /// </summary>
  public partial class MainWindow : Window {

    public new string Title {
      get {
        return base.Title;
      }
      set {
        base.Title = value;
        captionLabel.Content = value;
      }
    }

    public MainWindow() {
      InitializeComponent();
      ImageSource icon;
      if(this.Icon == null) {
        using(System.IO.MemoryStream s = new System.IO.MemoryStream()) {
          SystemIcons.Application.Save(s);
          icon = BitmapFrame.Create(s);
        }
      } else {
        icon = this.Icon;
      }
      imageAppIcon.Source = icon;
      this.Title = this.Title;
      ChangeMize(this.WindowState != System.Windows.WindowState.Normal);
    }

    #region [caption]
    private void captionBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
      if(e.ButtonState == MouseButtonState.Pressed) {
        if(e.ClickCount == 2) {
          ChangeMize(this.WindowState == System.Windows.WindowState.Normal);
        } else {
          this.DragMove();
        }
      }
    }
    #endregion

    #region [state change]
    private void ChangeMize(bool normal) {
      if(normal) {
        Dispatcher.BeginInvoke(DispatcherPriority.Input,
            (ThreadStart)delegate() {
          Screen screen = Screen.FromHandle(new WindowInteropHelper(this).Handle);
          this.WindowState = System.Windows.WindowState.Maximized;
          this.buttonMaximize.Visibility = System.Windows.Visibility.Collapsed;
          this.buttonNormalize.Visibility = System.Windows.Visibility.Visible;
        });
      } else {
        this.WindowState = System.Windows.WindowState.Normal;
        this.buttonMaximize.Visibility = System.Windows.Visibility.Visible;
        this.buttonNormalize.Visibility = System.Windows.Visibility.Collapsed;
      }
    }

    private void buttonMinimize_Click(object sender, RoutedEventArgs e) {
      this.WindowState = System.Windows.WindowState.Minimized;
    }

    private void buttonNormalize_Click(object sender, RoutedEventArgs e) {
      ChangeMize(false);
    }

    private void buttonMaximize_Click(object sender, RoutedEventArgs e) {
      ChangeMize(true);
    }

    private void buttonClose_Click(object sender, RoutedEventArgs e) {
      this.Close();
    }
    #endregion

    #region [border size change]
    private void rootBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
    }

    private void rootBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
    }

    private void rootBorder_MouseMove(object sender, System.Windows.Input.MouseEventArgs e) {
    }
    #endregion
  }
}
