﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace WpfApplication1 {
  /// <summary>
  /// MainWindow.xaml の相互作用ロジック
  /// </summary>
  public partial class MainWindow : Window {
    public MainWindow() {
      InitializeComponent();
    }

    private void Window_Loaded(object sender, RoutedEventArgs e) {
      Style style = null;
      if(Microsoft.Windows.Shell.SystemParameters2.Current.IsGlassEnabled) {
        Debug.WriteLine("---styles-----");
        foreach(DictionaryEntry r in Resources) {
          Debug.WriteLine(string.Format("{0} key({1}) val({2})", new object[] { r, r.Key, r.Value }));
        }
        style = (Style)Resources["GadgetStyle"];
        Debug.WriteLine("---styles-----");
      }
      this.Style = style;
    }
  }
}
